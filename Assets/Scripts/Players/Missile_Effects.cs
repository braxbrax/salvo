﻿using UnityEngine;
using System.Collections;

public class Missile_Effects : MonoBehaviour {

    public float spinSpeed;

    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 0, spinSpeed));
    }
}
