﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public Runner_Player runner;
	public Missile_Player missile;
    public float spawnTime;

    private Main_Player avatar;
	private PlayerView view;
	private PlayerArtPack artPack;
	private int uid;
	private int controlIdx;
	private bool isLocal;
    private bool respawning;
    private int score;
    private int stock;

	#region Properties

    public Main_Player Avatar { get { return avatar; } set { avatar = value; } }
	public PlayerView View { get { return view; } set { view = value; } }
	public PlayerArtPack ArtPack { get { return artPack; } set { artPack = value; } }
	public int Uid { get { return uid; } set { uid = value; } }
	public int ControlIdx { get { return controlIdx; } set { controlIdx = value; } }
	public bool IsLocal { get { return isLocal; } set { isLocal = value; } }
    public int Score { get { return score; } set { score = value; } }
    public int Stock { get { return stock; } set { stock = value; } }

	#endregion

	#region Initialization

	public void Init(int uid, bool isLocal, int controlIdx = -1, PlayerArtPack artPack = null,
		PlayerView view = null, Main_Player avatar = null, int score = 0, int stock = 0)
	{
		this.uid = uid;
		this.isLocal = isLocal;
		this.controlIdx = controlIdx;
		this.artPack = artPack;
		this.view = view;
        this.avatar = avatar;
        this.score = score;
        this.stock = stock;
        respawning = false;

        view.cam.GetComponent<SmoothFollow>().target = missile.transform;
        runner.gameObject.GetComponent<ThirdPersonCamera>().cameraTransform = view.cam.transform;
        runner.gameObject.GetComponent<ThirdPersonController>().cameraTransform = view.cam.transform;

		missile.mR.material.mainTexture = artPack.missileSkin;
	}

	#endregion

	public IEnumerator Respawn()
	{
        yield return new WaitForEndOfFrame();

        if (GamePlayManager.Instance.EndMatch == false)
        {

            respawning = true;

            yield return new WaitForSeconds(spawnTime);
            GamePlayManager.Instance.SpawnPlayer(this);

            respawning = false;
        }
	}

	public void Reset()
	{
        respawning = false;
	}
}