﻿using UnityEngine;
using System.Collections;

public class PlayerView : Menu
{
    public Texture radarTexture;

	public Camera cam;
	
	private Player player;


    //player.Uid is the index of the player 0-3

    //function borrowed from unity resource forums
    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return -1f;
        }
        else
        {
            return 1f;
        }
    }

    //I forgot what LF stands for
    float LFModifier(int playerIndex)
    {
        return AngleDir(player.Avatar.transform.forward, GamePlayManager.Instance.PlayerList[playerIndex].Avatar.transform.position - player.Avatar.transform.position, player.Avatar.transform.up);
    }

    /// <summary>
    /// Draws the dynamic directional radar at the specified coordinates
    /// </summary>
    /// <param name="pX">X coordinate</param>
    /// <param name="pY">Y coordinate</param>
    /// <param name="pDiameter">Radar diameter</param>
    void DrawRadar(float pX, float pY, float pDiameter)
    {
        GUI.DrawTexture(new Rect(pX, pY, pDiameter, pDiameter), radarTexture, ScaleMode.StretchToFill, true, 0);
        GUI.DrawTexture(new Rect(pX + (pDiameter / 2) - 7.5f, pY + (pDiameter / 2) - 7.5f, 15, 15), player.ArtPack.marker, ScaleMode.StretchToFill, true, 0);
        //player 1
        if(player.Uid == 0)
        {
            float theta = LFModifier(1) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[1].ArtPack.marker, ScaleMode.StretchToFill, true, 0);

            if(GamePlayManager.Instance.PlayerList.Count > 2)
            {
                theta = LFModifier(2) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
                GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[2].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            }
            if (GamePlayManager.Instance.PlayerList.Count > 3)
            {
                theta = LFModifier(3) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
                GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[3].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            }
        }
        //player 2
        else if (player.Uid == 1)
        {
            float theta = LFModifier(0) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[0].ArtPack.marker, ScaleMode.StretchToFill, true, 0);

            if (GamePlayManager.Instance.PlayerList.Count > 2)
            {
                theta = LFModifier(2) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
                GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[2].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            }
            if (GamePlayManager.Instance.PlayerList.Count > 3)
            {
                theta = LFModifier(3) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
                GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[3].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            }
        }
        //player 3
        else if (player.Uid == 2)
        {
            float theta = LFModifier(0) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[0].ArtPack.marker, ScaleMode.StretchToFill, true, 0);

            theta = LFModifier(1) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[1].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            if (GamePlayManager.Instance.PlayerList.Count > 3)
            {
                theta = LFModifier(3) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[3].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
                GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[3].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
            }
        }
        //player 4
        else if (player.Uid == 3)
        {
            float theta = LFModifier(0) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[0].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[0].ArtPack.marker, ScaleMode.StretchToFill, true, 0);

            theta = LFModifier(1) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[1].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[1].ArtPack.marker, ScaleMode.StretchToFill, true, 0);

            theta = LFModifier(2) * Vector3.Angle(new Vector3(player.Avatar.transform.forward.x, 0, player.Avatar.transform.forward.z), new Vector3(GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.x, 0, GamePlayManager.Instance.PlayerList[2].Avatar.transform.position.z) - new Vector3(player.Avatar.transform.position.x, 0, player.Avatar.transform.position.z));
            GUI.DrawTexture(new Rect(pX + (float)((pDiameter / 2) + (pDiameter / 2) * System.Math.Cos((theta + 90) * Mathf.Deg2Rad) - 7.5f), pY + (float)((pDiameter / 2) - (pDiameter / 2) * System.Math.Sin((theta + 90) * Mathf.Deg2Rad) - 7.5f), 15, 15), GamePlayManager.Instance.PlayerList[2].ArtPack.marker, ScaleMode.StretchToFill, true, 0);
        }
    }

    /// <summary>
    /// Draws the hud elements based on accepted parameters
    /// </summary>
    /// <param name="pX">The leftmost hud coordinate</param>
    /// <param name="pY">The topmost hud coordinate</param>
    /// <param name="pWidth">The width of the hud frame</param>
    /// <param name="pHeight">The height of the hud frame</param>
    void DrawHud(float pX, float pY, float pWidth, float pHeight)
    {
        GUI.BeginGroup(new Rect(pX, pY, pWidth, pHeight));
        //score
        GUI.DrawTexture(new Rect(pHeight / 40, 35 * pHeight / 40, 20 * pHeight / 40, 4 * pHeight / 40), player.ArtPack.playerSelectFrame, ScaleMode.StretchToFill, true, 0);
        GUI.Label(new Rect(2 * pHeight / 40, 33 * pHeight / 40, 20 * pHeight / 40, 4 * pHeight / 40), "Score");
        GUI.Label(new Rect(3 * pHeight / 40, 71 * pHeight / 80, 20 * pHeight / 40, 4 * pHeight / 40), player.Score + "");
        //radar
        DrawRadar(pWidth - (16 * pHeight / 40), pHeight - (16 * pHeight / 40), 15 * pHeight / 40);
        GUI.EndGroup();
    }

	#region Properties

	public Player Player { get { return player; } set { player = value; } }

	#endregion

	#region Initialization

	public void Init(Player player)
	{
		this.player = player;
	}

	#endregion

	/// <summary>
	/// Displays all of the GUI components for this PlayerView.
	/// </summary>
	public override void Display()
	{
        //if player 1
        if(player.Uid == 0)
        {
            //3, 2 total players
            if(GamePlayManager.Instance.PlayerList.Count < 4)
            {
                DrawHud(0.0f, 0.0f, Screen.width, Screen.height/2);
            }
            //4 total players
            else
            {
                DrawHud(0.0f, 0.0f, Screen.width / 2, Screen.height / 2);
            }
        }
        //if player 2
        if (player.Uid == 1)
        {
            //2 total players
            if (GamePlayManager.Instance.PlayerList.Count < 3)
            {
                DrawHud(0.0f, Screen.height / 2, Screen.width, Screen.height / 2);
            }
            //3 total players
            else if (GamePlayManager.Instance.PlayerList.Count < 4)
            {
                DrawHud(0.0f, Screen.height / 2, Screen.width / 2, Screen.height / 2);
            }
            //4 total players
            else
            {
                DrawHud(Screen.width / 2, 0.0f, Screen.width / 2, Screen.height / 2);
            }
        }
        //if player 3
        if (player.Uid == 2)
        {
            //3 total players
            if (GamePlayManager.Instance.PlayerList.Count < 4)
            {
                DrawHud(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2);
            }
            //4 total players
            else
            {
                DrawHud(0.0f, Screen.height / 2, Screen.width / 2, Screen.height / 2);
            }
        }
        //if player 4
        if (player.Uid == 3)
        {
            DrawHud(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2);
        }
	}
}