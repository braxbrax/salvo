﻿using UnityEngine;
using System.Collections;

public class Missile_Player : Main_Player
{
    private const int ENVIORNMENT = 8;

	public float standardSpeed;
	public float boostSpeed;
    public float rotateSpeed;
    public float blastRadius;
    public MeshRenderer mR;
	public Detonator det;

    private Vector3 direction;
    private Vector3 directionalInput;
	private float speed = 50;

	/*
    #region Temp way to find player
    GameObject line;
    LineRenderer lr;
    void Start()
    {
        line = new GameObject();
        GameObject.Instantiate(line);
        line.AddComponent("LineRenderer");
        lr = line.GetComponent<LineRenderer>();
    }
    #endregion
    */
    void PlayerInput()
    {
		directionalInput = new Vector3(Input.GetAxis("Control" + player.ControlIdx + "XAxis"), Input.GetAxis("Control" + player.ControlIdx + "YAxis"), 0);

		if (Input.GetButton("Control" + player.ControlIdx + "Run"))
		{
			speed = boostSpeed;
		}
		else
		{
			speed = standardSpeed;
		}

        //self destruct
		if (Input.GetButton("Control" + player.ControlIdx + "Action"))
        {
            DetectPlayerHit();
            Destroyed();
        }
    }

	// Update is called once per frame
	void Update () 
    {
        if (!destroyed)
        {
            PlayerInput();
        }

		/*
        #region Temp way to find player
        lr.SetPosition(0, transform.position);

        for (int i = 0; i < GamePlayManager.Instance.PlayerList.Count; i++)
        {
            if (GamePlayManager.Instance.PlayerList[i].Avatar == GamePlayManager.Instance.PlayerList[i].runner)
            {
                lr.SetPosition(1, GamePlayManager.Instance.PlayerList[i].Avatar.gameObject.transform.position);
            }
        }

        lr.SetColors(Color.red, Color.red);
        #endregion
        */
    }

    void FixedUpdate()
    {
        if (!destroyed)
        {
            direction = transform.forward * speed;

            rigidbody.velocity = direction + Vector3.ClampMagnitude(rigidbody.velocity, 10);

            if (directionalInput.sqrMagnitude > 0.1f)
            {
                transform.Rotate(Vector3.up, directionalInput.x * rotateSpeed);
                transform.Rotate(Vector3.right, directionalInput.y * rotateSpeed);
            }
        }

    }
    void OnCollisionEnter(Collision col)
    {
        DetectPlayerHit();
        Destroyed();
    }

    public override void Destroyed()
    {
        mR.enabled = false;

		Instantiate(det,transform.position,Quaternion.identity);

        for (int i = 0; i < transform.childCount; i++ )
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        destroyed = true;
        rigidbody.isKinematic = true;

        StartCoroutine(player.Respawn());
    }

    public override void Respawned()
    {
        mR.enabled = true;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }

        destroyed = false;
        rigidbody.isKinematic = false;

        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.velocity = Vector3.zero;
    }

    //line cast to all players, if no obstruction in line AND within blast radius, destroy that player
    void DetectPlayerHit()
    {
        foreach (Player pl in GamePlayManager.Instance.PlayerList)
        {
            Main_Player mp = pl.Avatar;

            if (!mp.gameObject.Equals(this.gameObject))
            {
                RaycastHit hit;

                Physics.Linecast(transform.position, mp.gameObject.transform.position, out hit);

                if (hit.collider == null) return;

                if (hit.collider.tag.Equals("Player") && (hit.distance <= blastRadius))
                {
                    mp.Destroyed();
                }
            }
        }
    }
}
