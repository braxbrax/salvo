﻿using UnityEngine;
using System.Collections;

public class PlayerArtPack : MonoBehaviour
{
	public int uid;
	public Texture playerSelectFrame;
    public Texture missileSkin;
    public Texture runnerSkin;
    public Texture marker;
}