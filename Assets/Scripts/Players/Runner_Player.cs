﻿using UnityEngine;
using System.Collections;

public class Runner_Player : Main_Player {

	public SkinnedMeshRenderer playerRender;

    void Start()
    {
        GetComponent<ThirdPersonController>().controlIdx = player.ControlIdx;
    }

    void Update()
    {
        if (GamePlayManager.Instance.EndMatch == false)
        {
            player.Score += (int)(100 * Time.deltaTime);
        }
    }

    public override void Destroyed()
    {
        animation.Stop();
        playerRender.enabled = false;
        GetComponent<ThirdPersonController>().enabled = false;
        destroyed = true;
    }

    public override void Respawned()
    {
        playerRender.enabled = true;
        GetComponent<ThirdPersonController>().enabled = true;
        destroyed = false;
    }
}
