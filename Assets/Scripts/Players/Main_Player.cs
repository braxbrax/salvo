﻿using UnityEngine;
using System.Collections;

public class Main_Player : MonoBehaviour
{
	public Player player;
    public bool destroyed;

	public virtual void Destroyed()
    {
        rigidbody.isKinematic = true;
    }

    public virtual void Respawned()
    {
        rigidbody.isKinematic = false;
    }

    void OnTriggerExit(Collider col)
    {
        Destroyed();
    }
}
