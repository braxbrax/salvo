using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameplayMenu : Menu
{
	/// <summary>
	/// Displays all of the GUI components for this GameplayMenu.
	/// </summary>
	public override void Display()
	{
		List<PlayerView> localPlayerViews = new List<PlayerView>();
		
		for (int i = 0; i < GamePlayManager.Instance.PlayerList.Count; ++i)
		{
			if (GamePlayManager.Instance.PlayerList[i].IsLocal)
			{
				localPlayerViews.Add(GamePlayManager.Instance.PlayerList[i].View);
			}
		}

		switch (localPlayerViews.Count)
		{
			case 1:
				localPlayerViews[0].cam.rect = new Rect(0f, 0f, 1f, 1f);
				break;

			case 2:
				localPlayerViews[0].cam.rect = new Rect(0f, 0.5f, 1f, 0.5f);
				localPlayerViews[1].cam.rect = new Rect(0f, 0f, 1f, 0.5f);
				break;

			case 3:
				localPlayerViews[0].cam.rect = new Rect(0f, 0.5f, 1f, 0.5f);
				localPlayerViews[1].cam.rect = new Rect(0f, 0f, 0.5f, 0.5f);
				localPlayerViews[2].cam.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
				break;

			case 4:
				localPlayerViews[0].cam.rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
				localPlayerViews[1].cam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
				localPlayerViews[2].cam.rect = new Rect(0f, 0f, 0.5f, 0.5f);
				localPlayerViews[3].cam.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
				break;
		}

		for (int i = 0; i < localPlayerViews.Count; ++i)
		{
			localPlayerViews[i].Display();
		}
	}
}