﻿using UnityEngine;
using System.Collections;

public class LoadingMenu : Menu
{
	/// <summary>
	/// Displays all of the GUI components for this LoadingMenu.
	/// </summary>
	public override void Display()
	{
		// Loading Window
		GUI.Box(new Rect(Screen.width / 2.0f - 50.0f, Screen.height / 2.0f - 30.0f, 100.0f, 60.0f), "Loading...");
	}
}