﻿using UnityEngine;
using System.Collections;

public class InstructionsMenu : Menu
{
    public Texture instructionMenu;
    public Texture backButton;
    /// <summary>
    /// Displays all of the GUI components for this MainMenu.
    /// </summary>
    public override void Display()
    {
        // Main Menu Window
        GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), instructionMenu, ScaleMode.ScaleToFit, true, 0);
        // Standard Mode Button
        if (GUI.Button(new Rect(60.0f, 300.0f, 120.0f, 60.0f), backButton))
        {
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
        }

        GUI.EndGroup();
    }
}