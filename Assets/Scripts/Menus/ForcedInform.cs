﻿using UnityEngine;
using System.Collections;

public class ForcedInform : Menu
{
    public Texture instructionMenu;
    public Texture skipButton;

	public AudioClip gameplayMusic1;
	public AudioClip gameplayMusic2;
	public AudioClip gameplayMusic3;
	public AudioClip gameplayMusic4;

    

    /// <summary>
    /// Displays all of the GUI components for this MainMenu.
    /// </summary>
    public override void Display()
    {
        // Main Menu Window
        GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), instructionMenu, ScaleMode.ScaleToFit, true, 0);
        // Standard Mode Button
        if (GUI.Button(new Rect(360.0f, 300.0f, 180.0f, 60.0f), skipButton))
        {
            if(GamePlayManager.Instance.levelLoad == 1)
            {
				GameManager.Instance.audio.clip=gameplayMusic1;
				GameManager.Instance.audio.Play();

				GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], new Survival() );
                GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
            }
            else if(GamePlayManager.Instance.levelLoad == 2)
            {
				GameManager.Instance.audio.clip=gameplayMusic2;
				GameManager.Instance.audio.Play();

				GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], new Conversion());
                GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
            }
            else if (GamePlayManager.Instance.levelLoad == 3)
            {
				GameManager.Instance.audio.clip=gameplayMusic3;
				GameManager.Instance.audio.Play();

				GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], new Infection());
                GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
            }
            else
            {
				GameManager.Instance.audio.clip=gameplayMusic4;
				GameManager.Instance.audio.Play();

				GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], new Overlord());
                GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
            }

            
        }

        GUI.EndGroup();
    }
}