﻿using UnityEngine;
using System.Collections;

public class ResultsMenu : Menu
{
    public Texture resultMenu;

    public Player place1;
    int max = 0;

	public AudioClip menuMusic;

    bool placesChecked = false;
    int scenario = 0;
	/// <summary>
	/// Displays all of the GUI components for this GameplayMenu.
	/// </summary>
    /// 

    //checks which of the competing players won
    void Placement()
    {
        //puts players in their place based on score
        foreach (Player piece in GamePlayManager.Instance.PlayerList)
        {
            Debug.Log(piece.Score);
            if (piece.Score > max)
            {
                Debug.Log(piece.Score);
                place1 = piece;
                max = piece.Score;
            }
        }
    }

    void ResetScores()
    {
        foreach (Player piece in GamePlayManager.Instance.PlayerList)
        {
            piece.Score = 0;
        }
        placesChecked = false;
        max = 0;
    }

	public override void Display()
	{
        //if players have not been placed yet, they will be
        if(!placesChecked)
        {
            Placement();
            max = 0;
            placesChecked = true;
        }

		// results Window
        GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));
        GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), place1.ArtPack.playerSelectFrame, ScaleMode.StretchToFill, true, 0);


        if (GUI.Button(new Rect((Screen.width / 2) - 250, (Screen.height / 2) - 200, 500, 100), "Rematch"))
        {
            GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], GamePlayManager.Instance.GameMode);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = false;
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
            ResetScores();
            GameManager.Instance.GameState = GameState.Gameplay;

        }
        if (GUI.Button(new Rect((Screen.width / 2) - 250, (Screen.height / 2) - 50, 500, 100), "Player Select"))
        {
			GameManager.Instance.audio.clip=menuMusic;
			GameManager.Instance.audio.Play();
            ResetScores();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }
        if (GUI.Button(new Rect((Screen.width / 2) - 250, (Screen.height / 2) + 100, 500, 100), "Main Menu"))
        {
			GameManager.Instance.audio.clip=menuMusic;
			GameManager.Instance.audio.Play();
            ResetScores();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
        }
	
		GUI.EndGroup();
	}
}
