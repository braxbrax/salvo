﻿using UnityEngine;
using System.Collections;

public class MainMenu : Menu
{
    public Texture title;
    public Texture splash;

    //for active button
    bool localPlayActive = true;
    bool networkPlayActive = false;
    bool quitActive = false;

    //for button advancer
    bool localPlayClicked = false;
    bool networkPlayClicked = false;
    bool quitClicked = false;

    //for gui delay
    float guiTimeStart = 0;
    bool guiReady = true;
    bool guiClicked = false;

    /// <summary>
    /// resets variables to initial values so that the button works as intended
    /// </summary>
    void Reinitialize()
    {
       localPlayActive = true;
       networkPlayActive = false;
       quitActive = false;

       localPlayClicked = false;
       networkPlayClicked = false;
       quitClicked = false;

       guiReady = true;
       guiTimeStart = 0;
       guiClicked = false;
    }

    /// <summary>
    /// since menus are nearly impossible to proprerly navigate when using axis controls, this handles a time delay between each accepted input
    /// </summary>
    void TransitHandler()
    {
        if (guiClicked && guiReady)
        {
            guiReady = false;
            guiTimeStart = Time.time;
        }
        else
        {
            if(Time.time - guiTimeStart > 0.2f)
            {
                guiReady = true;
                guiClicked = false;
            }
        }
    }

    /// <summary>
    /// update loop for all controller input
    /// </summary>
    void Update()
    {
        //action button
        if (localPlayActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            localPlayClicked = true;
        }
        if (networkPlayActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            networkPlayClicked = true;
        }
        if (quitActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            quitClicked = true;
        }
        //y axis
        if (guiReady && (Input.GetAxis("Control0YAxis") < -.01f || Input.GetAxis("Control1YAxis") < -.01f || Input.GetAxis("Control2YAxis") < -.01f || Input.GetAxis("Control3YAxis") < -.01f))
        {
            guiClicked = true;
            if (localPlayActive) { localPlayActive = false; networkPlayActive = true; }
            else if (networkPlayActive) { networkPlayActive = false; quitActive = true; }
        }
        if (guiReady && (Input.GetAxis("Control0YAxis") > .01f || Input.GetAxis("Control1YAxis") > .01f || Input.GetAxis("Control2YAxis") > .01f || Input.GetAxis("Control3YAxis") > .01f))
        {
            guiClicked = true;
            if (networkPlayActive) { networkPlayActive = false; localPlayActive = true; }
            else if (quitActive) { quitActive = false; networkPlayActive = true; }
        }
    }

    /// <summary>
    /// checks if any specific button-related controller-specific variables have been flipped and advances the scene if so
    /// </summary>
    void ButtonAdvancer()
    {
        if(localPlayClicked)
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }
        if (networkPlayClicked)
        {
            //reinitialize();
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }
        if (quitClicked)
        {
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }
    }

    /// <summary>
    /// on mouse hover: sets button to active
    /// </summary>
    void HoverSetter()
    {
        //contains reads x from the bottom of the screen rather than the top like everything else in ongui ლ(ಠ益ಠლ)
        if (new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2) - Screen.height / 9, Screen.width / 4, Screen.height / 9).Contains(Input.mousePosition))
        {
            localPlayActive = true;
            networkPlayActive = false;
            quitActive = false;
        }
        if (new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2) - 2*(Screen.height / 9) - (Screen.height / 30), Screen.width / 4, Screen.height / 9).Contains(Input.mousePosition))
        {
            localPlayActive = false;
            networkPlayActive = true;
            quitActive = false;
        }
        if (new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2) - (3 * Screen.height / 9) - (2 * Screen.height / 30), Screen.width / 4, Screen.height / 9).Contains(Input.mousePosition))
        {
            localPlayActive = false;
            networkPlayActive = false;
            quitActive = true;
        }
    }

	/// <summary>
	/// Displays all of the GUI components for this MainMenu.
	/// </summary>
	public override void Display()
	{
        HoverSetter();

		// Main Menu Window
		GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));

        GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), splash, ScaleMode.StretchToFill, true, 0);
        GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 3, 427.0f, 142.0f), title, ScaleMode.ScaleToFit, true, 0);

		// buttons
        if (localPlayActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.6f); }
		if (GUI.Button(new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2), Screen.width / 4, Screen.height / 9), "Local Play"))
		{
            Reinitialize();
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
		}

        if (networkPlayActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.6f); }
        if (GUI.Button(new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2) + (Screen.height / 9) + (Screen.height / 30), Screen.width / 4, Screen.height / 9), "Network Play"))
		{
			//GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
		}

        if (quitActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.6f); }
        if (GUI.Button(new Rect(Screen.width * (2 / 3.0f), (Screen.height / 2) + (2 * Screen.height / 9) + (2 * Screen.height / 30), Screen.width / 4, Screen.height / 9), "Quit"))
        {
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }


        ButtonAdvancer();
        TransitHandler();
		GUI.EndGroup();
	}
}