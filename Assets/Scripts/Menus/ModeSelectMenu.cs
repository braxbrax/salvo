﻿using UnityEngine;
using System.Collections;

public class ModeSelectMenu : Menu
{
    public Texture splash;

    //for active button
    bool survivalActive = true;
    bool conversionActive = false;
    bool infectionActive = false;
    bool overlordActive = false;
    bool backActive = false;

    //for button advancer
    bool survivalClicked = false;
    bool conversionClicked = false;
    bool infectionClicked = false;
    bool overlordClicked = false;
    bool backClicked = false;

    //for gui delay
    float guiTimeStart = 0;
    bool guiReady = true;
    bool guiClicked = false;

    //for dynamic description
    string survivalDescription = "Missiles vs 1 Runner\n\nPoints are awarded to the Runner relative to the duration of their survival.\n\nRoles are swapped regularly upon the Runner's defeat.\n\nEach player will have the same number of turns as the Runner.\n\nThe player with the most points at the conclusion of the game is the winner.";
    string conversionDescription = "Roles are reversed on the defeat of a Runner.\n\nPoints are awarded to Runners relative to the duration of their survival.\n\nThe game ends after a prespecified time limit.\n\nThe player with the most points at the conclusion of the game is the winner.\n\nThis mode is not yet implemented.";
    string infectionDescription = "1 Missile vs Runners\n\nRunners respawn as Missiles\n\nPoints are awarded to Runners relative to the duration of their survival.\n\nRoles are swapped regularly upon the defeat of all runners.\n\nEach player will have the same number of turns as the initial Missile.\n\nThe player with the most points at the conclusion of the game is the winner.\n\nThis mode is not yet implemented.";
    string overlordDescription = "1 Missile vs Runners\n\nRunners attempt to capture points on the map while a single Missile player hunts them down.\n\nRunners win if they can successfully capture a prespecified number of points.\n\nThe Missile player wins if they can defend the points until a prespecified time limit is reached\n\nThis mode is not yet implemented.";
    string dynamicDescription = "Missiles vs 1 Runner\n\nPoints are awarded to the Runner relative to the duration of their survival.\n\nRoles are swapped regularly upon the Runner's defeat.\n\nEach player will have the same number of turns as the Runner.\n\nThe player with the most points at the conclusion of the game is the winner.";
    string backDescription = "Return to the Main Menu";

    /// <summary>
    /// resets variables to initial values so that the button works as intended
    /// </summary>
    void Reinitialize()
    {
        survivalActive = true;
        conversionActive = false;
        infectionActive = false;
        overlordActive = false;

        survivalClicked = false;
        conversionClicked = false;
        infectionClicked = false;
        overlordClicked = false;
        backClicked = false;

        guiReady = true;
        guiTimeStart = 0;
        guiClicked = false;
        backActive = false;

        dynamicDescription = "Missiles vs 1 Runner\n\nPoints are awarded to the Runner relative to the duration of their survival.\n\nRoles are swapped regularly upon the Runner's defeat.\n\nEach player will have the same number of turns as the Runner.\n\nThe player with the most points at the conclusion of the game is the winner.";
    }

    /// <summary>
    /// since menus are nearly impossible to proprerly navigate when using axis controls, this handles a time delay between each accepted input
    /// </summary>
    void TransitHandler()
    {
        if (guiClicked && guiReady)
        {
            guiReady = false;
            guiTimeStart = Time.time;
        }
        else
        {
            if (Time.time - guiTimeStart > 0.2f)
            {
                guiReady = true;
                guiClicked = false;
            }
        }
    }

    /// <summary>
    /// update loop for all controller input
    /// </summary>
    void Update()
    {
        //action button
        if (survivalActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            survivalClicked = true;
        }
        if (conversionActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            conversionClicked = true;
        }
        if (infectionActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            infectionClicked = true;
        }
        if (overlordActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            overlordClicked = true;
        }
        if (backActive && (Input.GetButtonUp("Control0Action") || Input.GetButtonUp("Control1Action") || Input.GetButtonUp("Control2Action") || Input.GetButtonUp("Control3Action")))
        {
            backClicked = true;
        }
        //reject button
        if (Input.GetButtonUp("Control0Reject") || Input.GetButtonUp("Control1Reject") || Input.GetButtonUp("Control2Reject") || Input.GetButtonUp("Control3Reject"))
        {
            backClicked = true;
        }
        //y axis
        if (guiReady && (Input.GetAxis("Control0YAxis") < -.01f || Input.GetAxis("Control1YAxis") < -.01f || Input.GetAxis("Control2YAxis") < -.01f || Input.GetAxis("Control3YAxis") < -.01f))
        {
            guiClicked = true;
            if (backActive) { backActive = false; survivalActive = true; dynamicDescription = survivalDescription; }
            else if (survivalActive) { survivalActive = false; conversionActive = true; dynamicDescription = conversionDescription; }
            else if (conversionActive) { conversionActive = false; infectionActive = true; dynamicDescription = infectionDescription; }
            else if (infectionActive) { infectionActive = false; overlordActive = true; dynamicDescription = overlordDescription; }
        }
        if (guiReady && (Input.GetAxis("Control0YAxis") > .01f || Input.GetAxis("Control1YAxis") > .01f || Input.GetAxis("Control2YAxis") > .01f || Input.GetAxis("Control3YAxis") > .01f))
        {
            guiClicked = true;
            if (overlordActive) { overlordActive = false; infectionActive = true; dynamicDescription = infectionDescription; }
            else if (infectionActive) { infectionActive = false; conversionActive = true; dynamicDescription = conversionDescription; }
            else if (conversionActive) { conversionActive = false; survivalActive = true; dynamicDescription = survivalDescription; }
            else if (survivalActive) { survivalActive = false; backActive = true; dynamicDescription = backDescription; }
        }
    }

    /// <summary>
    /// checks if any specific button-related controller-specific variables have been flipped and advances the scene if so
    /// </summary>
    void ButtonAdvancer()
    {
        if (survivalClicked)
        {
            Reinitialize();
            GamePlayManager.Instance.GameMode = new Survival();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }
        if (conversionClicked)
        {
        }
        if (infectionClicked)
        {
        }
        if (overlordClicked)
        {
        }
        if (backClicked)
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
        }
    }

    /// <summary>
    /// on mouse hover: sets button to active
    /// </summary>
    void HoverSetter()
    {
        //contains reads x from the bottom of the screen rather than the top like everything else in ongui ლ(ಠ益ಠლ)
        if (new Rect((5 * Screen.height / 35), 5 * (Screen.height / 35), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)).Contains(Input.mousePosition))
        {
            overlordActive = true;
            infectionActive = false;
            conversionActive = false;
            survivalActive = false;
            backActive = false;
            dynamicDescription = overlordDescription;
        }
        if (new Rect((5 * Screen.height / 35), 10 * (Screen.height / 35) + (5 * Screen.height / 35) * (1 / 3.0f), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)).Contains(Input.mousePosition))
        {
            overlordActive = false;
            infectionActive = true;
            conversionActive = false;
            survivalActive = false;
            backActive = false;
            dynamicDescription = infectionDescription;
        }
        if (new Rect((5 * Screen.height / 35), 15 * (Screen.height / 35) + (5 * Screen.height / 35) * (2 / 3.0f), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)).Contains(Input.mousePosition))
        {
            overlordActive = false;
            infectionActive = false;
            conversionActive = true;
            survivalActive = false;
            backActive = false;
            dynamicDescription = conversionDescription;
        }
        if (new Rect((5 * Screen.height / 35), 20 * (Screen.height / 35) + (5 * Screen.height / 35) * (3 / 3), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)).Contains(Input.mousePosition))
        {
            overlordActive = false;
            infectionActive = false;
            conversionActive = false;
            survivalActive = true;
            backActive = false;
            dynamicDescription = survivalDescription;
        }
        if (new Rect((5 * Screen.height / 35), Screen.height - (5 * Screen.height / 70), (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 70)).Contains(Input.mousePosition))
        {
            overlordActive = false;
            infectionActive = false;
            conversionActive = false;
            survivalActive = false;
            backActive = true;
            dynamicDescription = backDescription;
        }
    }

	/// <summary>
	/// Displays all of the GUI components for this ModeSelectMenu.
	/// </summary>
	public override void Display()
	{
        HoverSetter();

        //menus buttons
        GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));
        GUI.skin.box.wordWrap = true;

        GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), splash, ScaleMode.StretchToFill, true, 0);
        GUI.Box(new Rect((Screen.width / 2) + (5 * Screen.height / 70), 5 * (Screen.height / 35), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (25 * Screen.height / 35)), dynamicDescription);

		//buttons
        if (survivalActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.6f); }
        if (GUI.Button(new Rect((5 * Screen.height / 35), 5 * (Screen.height / 35), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)), "Survival"))
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }

        if (conversionActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.3f); }
        if (GUI.Button(new Rect((5 * Screen.height / 35), 10 * (Screen.height / 35) + (5 * Screen.height / 35) * (1 / 3.0f), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)), "Conversion"))
        {
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }

        if (infectionActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.3f); }
        if (GUI.Button(new Rect((5 * Screen.height / 35), 15 * (Screen.height / 35) + (5 * Screen.height / 35) * (2 / 3.0f), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)), "Infection"))
        {
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }

        if (overlordActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.3f); }
        if (GUI.Button(new Rect((5 * Screen.height / 35), 20 * (Screen.height / 35) + (5 * Screen.height / 35) * (3 / 3), Screen.width / 2 - (5 * Screen.height / 70 + 5 * Screen.height / 35), (5 * Screen.height / 35)), "Overlord"))
        {
            //GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }

        if (backActive) { GUI.backgroundColor = new Color(0.0f, 0.0f, 1.0f, 1.0f); }
        else { GUI.backgroundColor = new Color(1.0f, 1.0f, 1.0f, 0.3f); }
        if (GUI.Button(new Rect((5 * Screen.height / 35), 0.0f, (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 70)), "B Back"))
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
        }

        ButtonAdvancer();
        TransitHandler();
		GUI.EndGroup();
	}
}