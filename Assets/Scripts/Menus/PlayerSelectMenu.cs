﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSelectMenu : Menu
{
    public Texture splash;
    public Texture frame;

	private List<bool> playerReadyList;
	private List<Texture> playerDisplayList;

    bool backClicked = false;
    bool startClicked = false;

    /// <summary>
    /// resets variables to initial values so that the button works as intended
    /// </summary>
    void Reinitialize()
    {
        backClicked = false;
        startClicked = false;
        //playerReadyList.Clear();
        //playerDisplayList.Clear();
    }

    /// <summary>
    /// checks if any specific button-related controller-specific variables have been flipped and advances the scene if so
    /// </summary>
    void ButtonAdvancer()
    {
        if (backClicked)
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }
        if (startClicked)
        {
            if(playerReadyList.Count < 2)
            {
                startClicked = false;
            }
            else
            {
                Reinitialize();
                GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], GamePlayManager.Instance.GameMode);
                Camera.main.enabled = false;
                GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
                GameManager.Instance.GameState = GameState.Gameplay;
            }
        }
    }

    /// <summary>
    /// populates a list with the currently selected players
    /// </summary>
	void OnEnable()
	{
		playerReadyList = new List<bool>();
		playerDisplayList = new List<Texture>();

		for (int i = 0; i < GamePlayManager.Instance.PlayerList.Count; ++i)
		{
			playerReadyList.Add(false);
			playerDisplayList.Add(null);
		}
	}

    /// <summary>
    /// update loop for controller input
    /// </summary>
	void Update()
	{
        //reject button
        if (Input.GetButtonUp("Control0Reject") || Input.GetButtonUp("Control1Reject") || Input.GetButtonUp("Control2Reject") || Input.GetButtonUp("Control3Reject"))
        {
            backClicked = true;
        }
        //start button
        if (Input.GetButtonUp("Control0Start") || Input.GetButtonUp("Control1Start") || Input.GetButtonUp("Control2Start") || Input.GetButtonUp("Control3Start"))
        {
            startClicked = true;
        }
        //individual controller input
		for (int i = 0; i < 4; ++i)
		{
			if (Input.GetButtonUp("Control" + i + "Action"))
			{
				ProcessInput(i);
			}
		}
	}

    /// <summary>
    /// this gets called once for every Dispay (OnGUI) call and will set player display texture based on readiness
    /// </summary>
    void Setter()
    {
		for (int i = 0; i < GamePlayManager.Instance.PlayerList.Count; ++i)
		{
			if (playerReadyList[i])
			{
				if (playerDisplayList[i] == frame)
				{
					playerDisplayList[i] = GamePlayManager.Instance.PlayerList[i].ArtPack.playerSelectFrame;
				}
			}
			else
			{
				if (playerDisplayList[i] != frame)
				{
					playerDisplayList[i] = frame;
				}
			}
		}
    }

    /// <summary>
    /// The loop
    /// </summary>
    public override void Display()
    {
        // Main Menu Window
        GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));

        GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), splash, ScaleMode.StretchToFill, true, 0);

		// NOTE: The control index conditional statement for each ready button should be removed if we move to a more dynamic menu (where you don't just have 4 fixed slots).
		// Process player ready button input
		//p1 ready button
		if (GUI.Button(new Rect(Screen.height * (1 / 7.0f), (30 * Screen.height / 35), (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 35)), "Toggle Ready"))
		{
			ProcessInput((GamePlayManager.Instance.PlayerList.Count >= 1) ? GamePlayManager.Instance.PlayerList[0].ControlIdx : 0);
		}
		
		//p2 ready button
		if (GUI.Button(new Rect((Screen.height * (1 / 7.0f)) + ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (1 / 21.0f)), (30 * Screen.height / 35), (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 35)), "Toggle Ready"))
		{
			ProcessInput((GamePlayManager.Instance.PlayerList.Count >= 2) ? GamePlayManager.Instance.PlayerList[1].ControlIdx : 1);
		}
		
		//p3 ready button
		if (GUI.Button(new Rect((Screen.height * (1 / 7.0f)) + 2 * ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (2 / 21.0f)), (30 * Screen.height / 35), (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 35)), "Toggle Ready"))
		{
			ProcessInput((GamePlayManager.Instance.PlayerList.Count >= 3) ? GamePlayManager.Instance.PlayerList[2].ControlIdx : 2);
		}
		
		//p4 ready button
		if (GUI.Button(new Rect((Screen.height * (1 / 7.0f)) + 3 * ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (3 / 21.0f)), (30 * Screen.height / 35), (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 35)), "Toggle Ready"))
		{
			ProcessInput((GamePlayManager.Instance.PlayerList.Count >= 4) ? GamePlayManager.Instance.PlayerList[3].ControlIdx : 3);
		}

		Setter();

		// NOTE: The texture conditional statement for each display should be removed if we move to a more dynamic menu (where you don't just have 4 fixed slots).
		//p1 display
        GUI.DrawTexture(new Rect(
            Screen.height * (1 / 7.0f),
            5 * (Screen.height / 35),
            (Screen.width - Screen.height * (3 / 7.0f)) / 4,
            (25 * Screen.height / 35)), (playerDisplayList.Count >= 1) ? playerDisplayList[0] : frame, ScaleMode.StretchToFill, true, 0);

		//p2 display
        GUI.DrawTexture(new Rect(
            (Screen.height * (1 / 7.0f)) + ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (1 / 21.0f)),
            5 * (Screen.height / 35),
            (Screen.width - Screen.height * (3 / 7.0f)) / 4,
			(25 * Screen.height / 35)), (playerDisplayList.Count >= 2) ? playerDisplayList[1] : frame, ScaleMode.StretchToFill, true, 0);

		//p3 display
        GUI.DrawTexture(new Rect(
            (Screen.height * (1 / 7.0f)) + 2 * ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (2 / 21.0f)), 
            5 * (Screen.height / 35), 
            (Screen.width - Screen.height * (3 / 7.0f)) / 4,
			(25 * Screen.height / 35)), (playerDisplayList.Count >= 3) ? playerDisplayList[2] : frame, ScaleMode.StretchToFill, true, 0);

        //p4 display
        GUI.DrawTexture(new Rect(
            (Screen.height * (1 / 7.0f)) + 3 * ((Screen.width - Screen.height * (3 / 7.0f)) / 4) + (Screen.height * (3 / 21.0f)),
            5 * (Screen.height / 35),
            (Screen.width - Screen.height * (3 / 7.0f)) / 4,
			(25 * Screen.height / 35)), (playerDisplayList.Count >= 4) ? playerDisplayList[3] : frame, ScaleMode.StretchToFill, true, 0);

        if(playerReadyList.Count >=2)
        {
            //gameplay button
            if (GUI.Button(new Rect(Screen.height * (1 / 7.0f), (Screen.height / 2) - (5 * Screen.height / 70), (Screen.width - Screen.height * (10 / 35.0f)), (5 * Screen.height / 35)), "Ready! Press Start"))
            {
                Reinitialize();
                GamePlayManager.Instance.StartGame(GameManager.Instance.levelPrefabs[0], GamePlayManager.Instance.GameMode);
                Camera.main.enabled = false;
			    GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
			    GameManager.Instance.GameState = GameState.Gameplay;
            }
        }

        //back button
        if (GUI.Button(new Rect((5 * Screen.height / 35), 0.0f, (Screen.width - Screen.height * (3 / 7.0f)) / 4, (5 * Screen.height / 70)), "B Back"))
        {
            Reinitialize();
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.modeSelectMenu;
        }

        ButtonAdvancer();
        GUI.EndGroup();
    }

	private void ProcessInput(int controlIdx)
	{
		Player player = null;
		for (int i = 0; i < GamePlayManager.Instance.PlayerList.Count; ++i)
		{
			if (GamePlayManager.Instance.PlayerList[i].ControlIdx == controlIdx && GamePlayManager.Instance.PlayerList[i].IsLocal)
			{
				player = GamePlayManager.Instance.PlayerList[i];
				break;
			}
		}

		if (player != null)
		{
			int playerIdx = GamePlayManager.Instance.PlayerList.IndexOf(player);
			playerReadyList.RemoveAt(playerIdx);
			playerDisplayList.RemoveAt(playerIdx);
			GamePlayManager.Instance.DestroyPlayer(player);
		}
		else
		{
			PlayerArtPack artPack = null;
			for (int i = 0; i < GameManager.Instance.playerArtPackUsage.Count; ++i)
			{
				if (GameManager.Instance.playerArtPackUsage[i] == 0)
				{
					artPack = Instantiate(GameManager.Instance.playerArtPackPrefabs[i]) as PlayerArtPack;
					++GameManager.Instance.playerArtPackUsage[i];
					break;
				}
			}
			PlayerView view = Instantiate(GameManager.Instance.playerViewPrefab) as PlayerView;

			player = GamePlayManager.Instance.CreatePlayer(true, controlIdx, artPack, view);

			artPack.transform.parent = player.transform;
			view.Init(player);
			view.transform.parent = player.transform;

			playerReadyList.Add(true);
			playerDisplayList.Add(artPack.playerSelectFrame);
		}
	}
}