﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Survival : GameMode {
    private int runnerIndex;

    public override void StartGame() {
        List<Player> playerList = GamePlayManager.Instance.PlayerList;
        runnerIndex = 0;
        for (int i = 0; i < playerList.Count; ++i)
        {
            playerList[i].Stock = startStock; //set stock
            playerList[i].spawnTime = 3f; //set spawn time (later we should be able to change the spawn time on a per player basis)
            if (i == runnerIndex)
            {
                playerList[i].Avatar = playerList[i].runner;
                playerList[i].View.cam.GetComponent<SmoothFollow>().enabled = false;
               
            }
            else
            {
                playerList[i].Avatar = playerList[i].missile;
                playerList[i].View.cam.GetComponent<SmoothFollow>().enabled = true;
            }

            if (playerList[i].IsLocal)
            {
                playerList[i].View.cam.gameObject.SetActive(true);
            }

            GamePlayManager.Instance.level.SpawnPlayer(playerList[i]); //spawn the player
        }
    }

    public override void UpdateGame(){
        List<Player> playerList = GamePlayManager.Instance.PlayerList;

        int runnerCount = 0;
        for (int i = 0; i < playerList.Count; i++)
        { // count remaining runners
            if (playerList[i].Avatar == playerList[i].runner && playerList[i].runner.destroyed == false)
            {
                runnerCount++;
            }
        }

        if (runnerCount == 0)// is the runner dead?
        {
            runnerIndex++;

            if (runnerIndex < playerList.Count) //need a new runner
            {
                for (int i = 0; i < playerList.Count; ++i)
                {
                    playerList[i].Stock = startStock; //set stock
                    playerList[i].spawnTime = 3f; //set spawn time (later we should be able to change the spawn time on a per player basis)
                    if (i == runnerIndex)
                    {
                        playerList[i].Avatar = playerList[i].runner;
                        playerList[i].missile.gameObject.SetActive(false);
                        playerList[i].View.cam.GetComponent<SmoothFollow>().enabled = false;

                    }
                    else
                    {
                        playerList[i].Avatar = playerList[i].missile;
                        playerList[i].runner.gameObject.SetActive(false);
                        playerList[i].View.cam.GetComponent<SmoothFollow>().enabled = true;
                    }

                    if (playerList[i].IsLocal)
                    {
                        playerList[i].View.cam.gameObject.SetActive(true);
                    }

                    GamePlayManager.Instance.level.SpawnPlayer(playerList[i]); //spawn the player
                }
            }
            else //everyones been a runner
            {
                if (GamePlayManager.Instance.EndMatch == false) 
                { 
                    GamePlayManager.Instance.StartCoroutine(GamePlayManager.Instance.EndGame()); 
                }
                    
            }
        }
    }

}
