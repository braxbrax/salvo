﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class GameMode{
    protected int startStock = 2; 


	// Use this for initialization
    public abstract void StartGame();
	
	// Update is called once per frame
    public abstract void UpdateGame();
}
