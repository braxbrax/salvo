﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GamePlayManager : Singleton<GamePlayManager>
{
    #region members
    public Level level;
    public float matchTimeLength;   // Duration of match
    public int startStock;
    public int levelLoad;   // let's forcedInform know which level to load
    public AudioClip menuMusic;
    public AudioClip[] gameplayMusic;
    public AudioClip resultsMusic;

    private int musicIndex;

    private int lastPlayerUid;
    private Dictionary<int, Player> playerDict;   // Dictionary of all players that uses player uid as key
    private List<Player> playerList;   // List of all players in sequential order
    private float matchTimeLeft;
    private GameMode gameMode;

    private bool endMatch;
    #endregion

    #region Properties

    public Dictionary<int, Player> PlayerDict { get { return playerDict; } }
    public List<Player> PlayerList { get { return playerList; } }
    public float MatchTimeLeft { get { return matchTimeLeft; } set { matchTimeLeft = value; } }
    public GameMode GameMode { get { return gameMode; } set { gameMode = value; } }
    public bool EndMatch { get { return endMatch; } set { endMatch = value; } }
    #endregion

    #region Initialization

    /// <summary>
    /// Protected constructor that can't be used, forcing this object to be a singleton.
    /// </summary>
    protected GamePlayManager() { }

    void Awake()
    {
        lastPlayerUid = -1;
        playerDict = new Dictionary<int, Player>();
        playerList = new List<Player>();
        matchTimeLeft = 0f;
    }

    #endregion

    #region Player
    public Player CreatePlayer(bool isLocal, int controlIdx = -1, PlayerArtPack artPack = null,
        PlayerView view = null, Main_Player avatar = null, int score = 0, int stock = 0)
    {
        Player player = Instantiate(GameManager.Instance.playerPrefab) as Player;
        ++lastPlayerUid;
        player.Init(lastPlayerUid, isLocal, controlIdx, artPack, view, avatar, score, stock);
        PlayerDict.Add(lastPlayerUid, player);
        PlayerList.Add(player);
        return player;
    }

    public void DestroyPlayer(Player player)
    {
        --GameManager.Instance.playerArtPackUsage[player.ArtPack.uid];
        PlayerDict.Remove(player.Uid);
        PlayerList.Remove(player);
        Destroy(player.gameObject);
    }

    public void SpawnPlayer(Player player) { level.SpawnPlayer(player); }

    #endregion

    // Sets all values to default
    public void StartGame(Level lvl, GameMode _gameMode)
    {
        //create and save level gameobject
        level = Instantiate(lvl) as Level;
        MatchTimeLeft = matchTimeLength;

        gameMode = _gameMode;

        endMatch = false;

        if (gameMode == null)
        {
            gameMode = new Survival();
        }

        gameMode.StartGame();
        musicIndex = (int)Random.Range(0, gameplayMusic.Length);
        GameManager.Instance.audio.clip = gameplayMusic[musicIndex];
        GameManager.Instance.audio.Play();
    }

    public void GameUpdate()
    {
        MatchTimeLeft -= Time.deltaTime; //Update time remaining

        if (MatchTimeLeft <= 0f && !endMatch)
        { // Is there time left?
            StartCoroutine(EndGame());
        }


        gameMode.UpdateGame();
    }

    public IEnumerator EndGame()
    {
        endMatch = true;

        yield return new WaitForSeconds(5.0f);

        Destroy(level.gameObject);
        MatchTimeLeft = 0f;

        foreach (Player player in PlayerList)
        {
            player.Reset();
            player.Avatar.gameObject.SetActive(false);
            if (player.IsLocal)
            {
                player.View.cam.gameObject.SetActive(false);
            }
        }

        GameManager.Instance.audio.clip = resultsMusic;
        GameManager.Instance.audio.Play();

        //Camera.main.gameObject.SetActive(true); //we need to find a better way than disabling the gameobject

        #region Temp Camera fix
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = true;
        #endregion

        GUIManager.Instance.CurrentMenu = GUIManager.Instance.resultsMenu;
        GameManager.Instance.GameState = GameState.Menu;
    }



    //TODO: move spawner logic to a spawner class
    //TODO: implement gamemode logic
}
