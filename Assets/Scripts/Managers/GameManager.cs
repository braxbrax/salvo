﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GameState
{
	Menu,
	Gameplay
};

public class GameManager : Singleton<GameManager>
{
	public Player playerPrefab;
	public PlayerView playerViewPrefab;
	public List<PlayerArtPack> playerArtPackPrefabs;
	public List<int> playerArtPackUsage;
	public List<Level> levelPrefabs;

	private GameState gameState;

	#region Initialization

	/// <summary>
	/// Protected constructor that can't be used, forcing this object to be a singleton.
	/// </summary>
	protected GameManager() { }

	void Awake()
	{
		gameState = GameState.Menu;
	}

	// Sets the menu the game begins on
	void Start()
	{
		GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
	}

	#endregion

	#region Properties

	public GameState GameState { get { return gameState; } set { gameState = value; } }

	#endregion

	#region Update

	void Update()
	{
		switch (GameState)
		{
			case GameState.Menu:

				break;

			case GameState.Gameplay:
				GamePlayManager.Instance.GameUpdate();
				break;
		}
	}

	#endregion
}