﻿using UnityEngine;
using System.Collections;

public class GUIManager : Singleton<GUIManager>
{
	public Menu loadingMenu;
	public Menu mainMenu;
	public Menu instructionsMenu;
	public Menu optionsMenu;
	public Menu creditsMenu;
	public Menu modeSelectMenu;
	public Menu levelSelectMenu;
	public Menu playerSelectMenu;
	public Menu gameplayMenu;
	public Menu pauseMenu;
	public Menu resultsMenu;
    public Menu forcedInform;

	private Menu currentMenu;

	#region Initialization

	/// <summary>
	/// Protected constructor that can't be used, forcing this object to be a singleton.
	/// </summary>
	protected GUIManager() {}

	// while the game is starting, loadingMenu is active
	void Awake()
	{
		currentMenu = loadingMenu;
		currentMenu.gameObject.SetActive(true);
	}

	#endregion

	#region Properties

	public Menu CurrentMenu
	{
		get { return currentMenu; }

		set
		{
			currentMenu.gameObject.SetActive(false);
			currentMenu = value;
			currentMenu.gameObject.SetActive(true);
		}
	}

	#endregion

	/// <summary>
	/// Handles all GUI events and renders the appropriate GUI in the game.
	/// </summary>
	void OnGUI()
	{
		if (CurrentMenu != null)   // If a Menu should currently be displayed
		{
			CurrentMenu.Display();
		}
	}
}