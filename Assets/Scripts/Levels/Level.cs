﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Stores all of the data for a specific level in the game, as well as spawning logic
/// </summary>
public class Level : MonoBehaviour
{
	public string title;
	public List<Transform> runnerSpawnLocs;    // List of spawn locations for runners
	public List<Transform> missileSpawnLocs;   // List of spawn locations for missiles
	public List<Transform> pickupSpawnLocs;	   // List of spawn locations for pickups

    private float spawnRadius = 10;

    #region Spawning

    //select a spawn point and pass it to spawnPlayer overload
    public void SpawnPlayer(Player player) {
		//print("spawn player");
		List<Transform> spawns = (player.Avatar == player.runner) ? runnerSpawnLocs : missileSpawnLocs;
		int j = Random.Range(0, spawns.Count);
        for (int i = 0; i < spawns.Count; i++) {
			if (checkSpawn(spawns[j])) {
                SpawnPlayer(player, spawns[j]);
                return;
            }
			j = (j + 1) % spawns.Count;
        }
    }

    private void SpawnPlayer(Player player, Transform spawnPoint)
    {
        print("spawn player at location");
        //for spawning players to select location at game start/reset of game
        if (player.Avatar == player.runner)
        {
            player.Avatar.transform.CopyNoScale(spawnPoint.transform);
        }
        else
        {
            player.Avatar.transform.CopyNoScale(spawnPoint.transform);
        }

        player.Avatar.gameObject.SetActive(true);

        player.Avatar.Respawned();
    }

    //checks if the spawn point is free to spawn at
    private bool checkSpawn(Transform loc) {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++) {
            if (Vector3.Distance(loc.position, players[i].transform.position) < spawnRadius) { // a player exists within the spawn radius
                return false;
            }
        }
        return true;
    }
   
    #endregion
}